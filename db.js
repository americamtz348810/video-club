const Sequelize = require('sequelize');

const directorModel = require ('.models/director');
const genreModel = require('./models/genre');
const movieModel = require('./models/movie');
const actorModel = require('./models/actor');
const movieActorModel = require('./models/movieActor');
const memberModel = require('./models/member');
const copyModel = require('./models/copy');
const bookingModel = require('./models/booking');


/*
    1. Nombre de la base de datos
    2. Usuario de la base de datos
    3. Contraseña de la base de datos
    4. Objeto de configuración ORM

*/

const sequelize = new Sequelize('video-club-', 'root', 'hhq5q4c9', {
    host: '127.0.0.1',
    dialect: 'mysql'
});

const Director = directorModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const MovieActor = movieActorModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const Copy = copyModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);


//MOVIES

// Un género puede tener muchas películas
Genre.hasMany(Movie, {as: 'movies'});

// Una película tiene un género
Movie.belongsTo(Genre, {as: 'genre'})

//Un director puede tener muchas películas
Director.hasMany(Movie, {as:'movies'});

//Una película tiene un director
Movie.belongsTo(Director, {as: 'director'});

//MOVIES_ACTOR

// Un actor participa en muchas peliculas
MovieActor.belongsTo(Movie, {foreignKey: 'movieId'});

// En una película participan muchos actores
MovieActor.belongsTo(Actor, {foreignKey: 'actorId'});

MovieActor.belongsToMany(Actor, {
    foreignKey: 'actorID',
    as: 'actors',
    throught: 'movies_actors'
});

Actor.belongsToMany(Movie, {
    foreignKey: 'movieId',
    as: 'movies',
    throught: 'movies_actors'
});

// COPIES

// Una película puede tener muchas copias
Movie.hasMany(Copy, {as: 'copies'});

// Una copia solo es de una película
Copy.belongsTo(Movie, {as: 'movie'})


//BOOKINGS

// Una copia puede ser prestada varias veces
Copy.hasMany(Booking, {as: 'bookings'});

// Un préstamo solo abarca una 
Booking.belongsTo(Copy, {as: 'copy'})

// Un miembro puede tener varios préstamo
Member.hasMany(Booking, {as: 'bookings'});

// Un préstamo solo puede ser solicitado por solo un miembro
Booking.belongsTo(Member, {as: 'member'})


sequelize.sync({
    force:true
}).then(() => {
    console.log('Base de datos sicronizada.')
});

module.exports = (Director, Genre, Movie, Actor, Member, Copy, Booking);