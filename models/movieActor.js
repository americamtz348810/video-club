module.exports = (sequelize, type) => {
    const MovieActor = sequelize.define('movies_actors', {
        id : {type: type.INTEGER, primaryKe : true, autoIncrement :  true},
        movieID: type.INTEGER,
        actorId: type.INTEGER
    })
};